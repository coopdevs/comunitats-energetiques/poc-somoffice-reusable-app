# somoffice

## Installation

Install development environment following instructions in [somoffice-provisioning](https://gitlab.com/coopdevs/somoffice-provisioning).

## Running locally

Once you got your devenv up and running, ssh into it with

    ssh somoffice@local.somoffice.coop

and do the following.

### Backend

1. Install backend dependencies

```sh
cd /opt/somoffice
# cd /vagrant/somoffice if you are using vagrant

poetry install
```

2. Source environment variables

```sh
export $(cat /etc/default/somoffice | grep -v '^#' | xargs)
```

3. The very first time you start the backend, you'll need to run migations.

```sh
poetry run python manage.py migrate
```

4. Start the server

```sh
poetry run python manage.py runserver 0.0.0.0:8000
```

If you are working with emails, on another terminal you should run the following command.

```sh
python -m smtpd -n -c DebuggingServer localhost:1025
```

This will spin up a fake SMTP server which displays sent emails in text form in the terminal.

### Frontend

1. Install dependencies

```sh
cd /opt/somoffice/frontend
# cd /vagrant/somoffice/frontend if you are using vagrant
npm install
```

2. Start server

```sh
npm run start
```

After you have completed all steps listed in backend and frontend, you'll be able to access it on http://local.somoffice.coop:3000/

### Running tests

To run tests on backend you have to run:

```sh
poetry run pytest
```

#### HTTP requests in tests

This project uses [pytest-recording](https://pypi.org/project/pytest-recording/) to record and mock outgoing HTTP requests to 3rd party APIs.

If you are writing a new tests that is making requests, you should run:

```sh
poetry run pytest --record-mode=once path/to/your/test
```

You might need to record requests for an specific tests. In that case make sure to only run the tests affected and run

```sh
poetry run pytest --record-mode=rewrite path/to/your/test
```

## Architecture overview

Somoffice consists of the following:

* A back-end REST API powered by django.
* A front-end SPA (Single Page Application) powered by React.

Although is not quite there yet, one of the goals of the project is to serve as
a re-usable customer area framework for different clients/cooperatives.

### Backend stack and architecture

For the sake of re-usability, backend architecture revolves around the concept of providers. The goal of the providers is to talk to 3rd party APIs and _provide_ the application with data.

To achieve this it has been separated in two separate [django apps](https://docs.djangoproject.com/en/3.0/ref/applications/).

* somoffice: handles all HTTP stuff, including authorization and authentication. It is agnostic of any business logic and just talks to providers. It also exposes register hooks so you can connect a provider to an specific endpoint.
* somconnexio: this is where the business logic of Som Connexió lives. It implements _providers_ that talk to a myriad of APIs (opencell, otrs, keycloak, and more in the future).

### Frontend stack and architecture

There is currently no modular architecture in frontend, in the same way as _providers_ in backend. This might be done in a future iteration.
