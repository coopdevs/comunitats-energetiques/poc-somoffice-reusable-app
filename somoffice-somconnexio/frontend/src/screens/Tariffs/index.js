import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import { Card } from "components/Card";
import { Heading } from "components/Heading";
import { Subheading } from "components/Subheading";
import { Text } from "components/Text";
import { Button } from "components/Button";
import { Spinner } from "components/Spinner";
import { Stack } from "components/layouts/Stack";
import { IframeLayout } from "components/layouts/IframeLayout";
import { Container } from "components/layouts/Container";
import { Modal } from "components/Modal";
import { useTariffs } from "hooks/queries/useTariffs";
import { MobileTariffPicker } from "./MobileTariffPicker";
import { InternetTariffPicker } from "./InternetTariffPicker";
import { useHistory, useParams } from "react-router-dom";
import { Trans, useTranslation } from "react-i18next";
import { useStore } from "hooks/useStore";
import { Link } from "components/Link";
import { AddButton } from "../SignUp/shared/AddButton";
import { propagateLocaleParam } from "lib/propagateLocaleParam";

const getDefaultsFor = type => {
  if (type === "mobile") {
    return [{ type: "mobile" }];
  }

  if (type === "internet") {
    return [{ type: "internet" }];
  }

  if (type === "internet_and_mobile") {
    return [{ type: "internet" }, { type: "mobile" }];
  }
};

const RemoveLineButton = ({ index }) => {
  const { t } = useTranslation();
  const removeLineAt = useStore(state => state.removeLineAt);

  return (
    <Text
      onClick={() => removeLineAt(index)}
      size="2xs"
      bold
      color="primary.main"
      letterSpacing="0.1rem"
      uppercase
    >
      {t("funnel.shared.remove_service")}
    </Text>
  );
};

const LoginLink = ({ children, url }) => {
  return (
    <Text textAlign="center" bold underline color="primary.main">
      <Link target="_parent" to={url}>
        {children}
      </Link>
    </Text>
  );
};

const typesFromUrlParam = type => {
  if (type === "mobile") {
    return ["mobile"];
  }

  if (type === "internet") {
    return ["internet"];
  }

  if (type === "internet_and_mobile") {
    return ["internet", "mobile"];
  }
};

export const Tariffs = () => {
  const { t } = useTranslation();
  const { type } = useParams();
  const types = typesFromUrlParam(type);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const lines = useStore(state => state.lines);
  const setLines = useStore(state => state.setLines);
  const addInternetLine = useStore(state => state.addInternetLine);
  const addMobileLine = useStore(state => state.addMobileLine);
  const updateLineAt = useStore(state => state.updateLineAt);
  const internetLines = lines.filter(line => line.type === "internet");
  const mobileLines = lines.filter(line => line.type === "mobile");

  useEffect(() => setLines(getDefaultsFor(type)), [type]);

  const openConfirmModal = () => setIsModalOpen(true);

  const { isLoading, error, data } = useTariffs();

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return <span>{JSON.stringify(error)}</span>;
  }

  const getSignupUrl = (optingForRole = "member") => {
    const serializedIntent = JSON.stringify({
      optingForRole,
      lines
    });

    return propagateLocaleParam(
      `/signup/data?intent=${encodeURIComponent(serializedIntent)}`
    );
  };

  const onConfirm = () => {
    window.open(getSignupUrl(), "_parent");
  };

  const onWantsToBeSponsoredClick = () => {
    window.open(getSignupUrl("sponsored"), "_parent");
  };

  const getLoginUrl = () =>
    `/login?redirect_to=${encodeURIComponent(getSignupUrl())}`;

  return (
    <IframeLayout showBackgroundRect={false}>
      <Box mt={7} />
      <Container variant="narrow">
        <Heading>{t(`funnel.tariffs.${type}.title`)}</Heading>
        {internetLines.map((line, index) => (
          <Box key={line.__id} mt={5}>
            <Card
              topRightAction={index > 0 && <RemoveLineButton index={index} />}
            >
              <InternetTariffPicker
                onChange={tariff => updateLineAt(tariff, "internet", index)}
                tariffs={data}
              />
            </Card>
          </Box>
        ))}

        {types.includes("internet") && internetLines.length < 2 && (
          <Box mt={5}>
            <AddButton
              onClick={addInternetLine}
              text={t("funnel.tariffs.internet.add_line")}
            />
          </Box>
        )}

        {mobileLines.map((line, index) => (
          <Box key={line.__id} mt={5}>
            <Card
              topRightAction={
                index > 0 && (
                  <RemoveLineButton index={internetLines.length + index} />
                )
              }
            >
              <MobileTariffPicker
                onChange={tariff =>
                  updateLineAt(tariff, "mobile", internetLines.length + index)
                }
                tariffs={data}
              />
            </Card>
          </Box>
        ))}

        {types.includes("mobile") && mobileLines.length < 4 && (
          <Box mt={5}>
            <AddButton
              onClick={addMobileLine}
              text={t("funnel.tariffs.mobile.add_line")}
            />
          </Box>
        )}
      </Container>
      <Box minHeight="230px" bgcolor="secondary.main" mt={8}>
        <Container variant="narrow">
          <Box style={{ transform: "translateY(-50%)" }}>
            <Button onClick={openConfirmModal}>{t("common.contract")}</Button>
          </Box>
          <Box px={8}>
            <Text>
              <Trans i18nKey="funnel.tariffs.cta_footer">
                <Text bold />
              </Trans>
            </Text>
          </Box>
        </Container>
      </Box>
      <Modal isOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
        <Stack align="center" spacing={4}>
          <Text size="xl" lineHeight={1.25} bold>
            {t("funnel.tariffs.confirm_modal.title")}
          </Text>
          <Text>
            <Trans i18nKey="funnel.tariffs.confirm_modal.body">
              <Text textAlign="center" bold lineHeight={1.375} />
            </Trans>
          </Text>
          <Button onClick={onConfirm}>
            {t("funnel.tariffs.confirm_modal.cta")}
          </Button>
          <Text textAlign="center" color="primary.main">
            <Trans i18nKey="funnel.tariffs.confirm_modal.already_an_user_log_in">
              <LoginLink url={getLoginUrl()} />
            </Trans>
          </Text>
          {type === "mobile" && (
            <Text
              textAlign="center"
              onClick={onWantsToBeSponsoredClick}
              bold
              underline
              color="primary.main"
            >
              {t("funnel.tariffs.confirm_modal.sponsored")}
            </Text>
          )}
        </Stack>
      </Modal>
    </IframeLayout>
  );
};
