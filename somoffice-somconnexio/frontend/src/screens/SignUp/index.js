import { TopBarLayout } from "components/layouts/TopBarLayout";
import { styled, Box } from "@material-ui/core";
import React, { useEffect } from "react";
import { Route, Switch, useRouteMatch, useLocation } from "react-router-dom";
import { Tiles } from "components/layouts/Tiles";
import { Stepper } from "components/Stepper";
import { Spinner } from "components/Spinner";
import { Data } from "./Data";
import { Payment } from "./Payment";
import { Confirm } from "./Confirm";
import { Thanks } from "./Thanks";
import { useTranslation, Trans } from "react-i18next";
import { Container } from "components/layouts/Container";
import { Text } from "components/Text";
import { PaymentSummary } from "./PaymentSummary";
import { useTariffs } from "hooks/queries/useTariffs";
import { useSubscriptions } from "hooks/queries/useSubscriptions";
import { useApplicationContext } from "hooks/useApplicationContext";
import { reuseOpencellAddresses } from "lib/domain/somconnexio/reuseOpencellAddresses";
import { useStore } from "hooks/useStore";
import { compact, uniq, uniqueId } from "lodash";

const Wrapper = styled("div")(({ theme, hasSidebar }) => ({
  [theme.breakpoints.down("md")]: {
    paddingRight: 0
  },
  [theme.breakpoints.up("md")]: {
    paddingRight: hasSidebar ? "300px" : 0
  }
}));

export const SignUp = () => {
  const { path } = useRouteMatch();
  const { pathname } = useLocation();
  const { t } = useTranslation();

  const { currentUser } = useApplicationContext();
  const loggedIn = Boolean(currentUser);
  const steps = ["/signup/data", "/signup/payment", "/signup/confirm"];
  const shouldShowStepper = steps.includes(pathname);
  const shouldShowPaymentSummary = shouldShowStepper;
  const {
    isLoading: isTariffsLoading,
    error: tariffsError,
    data: tariffs
  } = useTariffs();
  const {
    isLoading: isSubscriptionsLoading,
    error: subscriptionError,
    data: subscriptions
  } = useSubscriptions({ enabled: loggedIn });
  const saveAddress = useStore(state => state.saveAddress);
  const setAvailablePaymentMethods = useStore(
    state => state.setAvailablePaymentMethods
  );
  const waitingForSubscriptions = loggedIn && isSubscriptionsLoading;
  const isLoading = isTariffsLoading || waitingForSubscriptions;

  useEffect(() => {
    function initializeAddresses() {
      reuseOpencellAddresses(
        compact(subscriptions.map(item => item.address))
      ).forEach(address => saveAddress(address));
    }

    function initializePaymentMethods() {
      setAvailablePaymentMethods(
        uniq(subscriptions.map(({ iban }) => iban)).map(iban => ({
          id: uniqueId(),
          type: "bank-account",
          iban
        }))
      );
    }

    if (!loggedIn || isSubscriptionsLoading) {
      return;
    }

    initializeAddresses();
    initializePaymentMethods();
  }, [loggedIn, isSubscriptionsLoading, saveAddress, subscriptions]);

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <TopBarLayout>
      <Wrapper hasSidebar={shouldShowPaymentSummary}>
        {shouldShowStepper && (
          <Container>
            <Box mt={8} mx={[0, 7]}>
              <Tiles columns={1} spacing={0}>
                <Text uppercase size="sm" letterSpacing="0.1rem">
                  {t("funnel.header.title")}
                </Text>
                <Box my={6}>
                  <Stepper
                    currentStep={pathname}
                    steps={steps.map(step => ({
                      key: step,
                      label: t("funnel.header.steps." + step.split("/")[2])
                    }))}
                  />
                </Box>
              </Tiles>
            </Box>
          </Container>
        )}
        <Switch>
          <Route path={`${path}/data`}>
            <Data tariffs={tariffs} />
          </Route>
          <Route path={`${path}/payment`}>
            <Payment />
          </Route>
          <Route path={`${path}/confirm`}>
            <Confirm tariffs={tariffs} />
          </Route>
          <Route path={`${path}/thanks`}>
            <Thanks />
          </Route>
        </Switch>
        {shouldShowPaymentSummary && <PaymentSummary tariffs={tariffs} />}
      </Wrapper>
    </TopBarLayout>
  );
};
