import React, { useEffect } from "react";
import { Container } from "components/layouts/Container";
import { Box } from "@material-ui/core";
import { Text } from "components/Text";
import { Tiles } from "components/layouts/Tiles";
import { JsonParam, useQueryParam } from "use-query-params";
import { useDerivedState, useStore } from "hooks/useStore";

import { LineStepGroup } from "./shared/StepGroups/LineStepGroup";
import { PersonalDataStepGroup } from "./shared/StepGroups/PersonalDataStepGroup";
import { useApplicationContext } from "hooks/useApplicationContext";
import { getLineGroupProps } from "./shared/getLineGroupProps";
import { AddButton } from "./shared/AddButton";
import groupBy from "lodash.groupby";
import { useTranslation } from "react-i18next";

const AddLine = ({ type }) => {
  const { t } = useTranslation();
  const { canAddLineOfType } = useDerivedState();
  const addMobileLine = useStore(state => state.addMobileLine);
  const addInternetLine = useStore(state => state.addInternetLine);
  const addCallback = type === "internet" ? addInternetLine : addMobileLine;
  const canAddLine = canAddLineOfType(type);
  const [intent] = useQueryParam("intent", JsonParam);
  const didNotWantAnyLines = (intent.lines || []).length === 0;

  if (!canAddLine || didNotWantAnyLines) {
    return null;
  }

  return (
    <AddButton
      onClick={addCallback}
      text={t(`funnel.tariffs.${type}.add_line`)}
    />
  );
};

export const Data = ({ tariffs }) => {
  const [intent] = useQueryParam("intent", JsonParam);
  const initializeSignupFlow = useStore(state => state.initializeSignupFlow);
  const lines = useStore(state => state.lines);
  const { internetLines = [], mobileLines = [] } = groupBy(
    lines,
    line => `${line.type}Lines`
  );
  const setLines = useStore(state => state.setLines);
  const { currentUser } = useApplicationContext();
  const loggedIn = Boolean(currentUser);
  const { currentStep } = useDerivedState();

  useEffect(() => {
    setLines(intent.lines);
  }, [intent, setLines]);

  useEffect(() => {
    if (lines.length > 0 && !lines[0].__id) {
      return;
    }

    initializeSignupFlow({
      loggedIn,
      role: intent.optingForRole || currentUser?.role,
      lines,
      steps: lines.flatMap(line => [
        `line-${line.__id}/tariff`,
        `line-${line.__id}/additional-data`
      ])
    });
  }, [lines, currentUser, loggedIn, intent, setLines, initializeSignupFlow]);

  useEffect(() => {
    const el = document.getElementById(`scroll/${currentStep}`);

    if (!el) {
      return;
    }

    const y = el.getBoundingClientRect().top + window.scrollY;

    window.scroll({
      top: y - 250,
      behavior: "smooth"
    });
  }, [currentStep]);

  return (
    <Container variant="narrow">
      <Tiles columns={1} spacing={5}>
        {!loggedIn && (
          <PersonalDataStepGroup
            optingForRole={intent.optingForRole}
            goToPayment={lines.length === 0}
          />
        )}
        {internetLines.map((line, index) => {
          const isLast = index === lines.length - 1;

          return (
            <LineStepGroup
              key={`line-${line.__id}`}
              line={line}
              stepsBefore={loggedIn ? 1 : 2}
              index={index}
              isLast={isLast}
              tariffs={tariffs}
              {...getLineGroupProps(index, lines)}
            />
          );
        })}

        <AddLine type="internet" />

        {mobileLines.map((line, index) => {
          const isLast = index === mobileLines.length - 1;

          return (
            <LineStepGroup
              key={`line-${line.__id}`}
              line={line}
              stepsBefore={loggedIn ? 1 : 2}
              index={index}
              isLast={isLast}
              tariffs={tariffs}
              {...getLineGroupProps(index, lines)}
              indexOfKind={index}
            />
          );
        })}

        <AddLine type="mobile" />
      </Tiles>
      {/* TODO some padding at the bottom to avoid scroll jumping when collapsing boxes */}
      <Box height="400px" />
    </Container>
  );
};
