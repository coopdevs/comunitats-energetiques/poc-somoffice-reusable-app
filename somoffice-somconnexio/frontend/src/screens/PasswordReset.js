import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import {
  Switch,
  Route,
  useParams,
  useRouteMatch,
  useHistory
} from "react-router-dom";
import Alert from "@material-ui/lab/Alert";

import Box from "@material-ui/core/Box";

import { FullScreenCenteredLayout } from "components/layouts/FullScreenCenteredLayout";
import {
  requestPasswordReset,
  validatePasswordResetToken,
  resetPassword
} from "lib/api/auth";

import { Spinner } from "components/Spinner";
import { LegacyTextField } from "components/LegacyTextField";
import { Button } from "components/Button";
import { Form } from "components/LegacyForm";
import { Link } from "components/Link";
import { Stack } from "components/layouts/Stack";

const parsePasswordValidationErrors = error => {
  return error.response.data.password.map(rawMessage => {
    return { message: rawMessage };
  });
};

const RequestPasswordReset = ({ redirectUrl }) => {
  const { t } = useTranslation();
  const [username, setUsername] = useState("");
  const [error, setError] = useState(null);
  const [isSent, setIsSent] = useState(false);

  async function onSubmitSendEmail() {
    try {
      await requestPasswordReset({ username });
      setIsSent(true);
    } catch (e) {
      setError(true);
      /* handle error */
    }
  }

  if (isSent) {
    return (
      <div>
        <p>{t("password_reset.email_sent")}</p>
      </div>
    );
  }

  return (
    <Form onSubmit={onSubmitSendEmail}>
      <Stack spacing={2}>
        <p>{t("password_reset.request")}</p>
        <LegacyTextField
          label={t("login.username")}
          value={username}
          setState={setUsername}
        />
        <Button type="submit">{t("password_reset.send_email")}</Button>
        {error && (
          <Box width="100%">
            <Alert severity="error">
              {t("password_reset.errors.could_not_send_email")}
            </Alert>
          </Box>
        )}
      </Stack>
    </Form>
  );
};

const PasswordValidationError = ({ error }) => {
  return <Alert severity="error">{error.message}</Alert>;
};

const SetNewPassword = () => {
  const history = useHistory();
  const { token } = useParams();
  const { t } = useTranslation();
  const [isTokenValid, setIsTokenValid] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const [passwordResetErrors, setPasswordResetErrors] = useState([]);
  const [passwordResetComplete, setPasswordResetComplete] = useState(false);

  async function onSubmitSetPassword() {
    if (password1 !== password2) {
      setPasswordResetErrors([
        { message: t("password_reset.errors.passwords_do_not_match") }
      ]);
      return;
    }

    try {
      setIsLoading(true);
      await resetPassword({ token, password: password1 });
      setPasswordResetComplete(true);
    } catch (error) {
      setPasswordResetErrors(parsePasswordValidationErrors(error));
      /* handle error */
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      try {
        await validatePasswordResetToken({ token });
        setIsTokenValid(true);
      } catch (e) {
        setIsTokenValid(false);
        /* handle error */
      } finally {
        setIsLoading(false);
      }
    })();
  }, [token]);

  if (isLoading) {
    return <Spinner />;
  }

  if (isTokenValid === false) {
    return <p>{t("password_reset.invalid_link")}</p>;
  }

  if (passwordResetComplete) {
    return (
      <Stack spacing={2} align="center">
        <Button onClick={() => history.push("/")}>
          {t("password_reset.go_to_login")}
        </Button>
        <Link to="/product-picker">{t("password_reset.more_services")}</Link>
      </Stack>
    );
  }

  return (
    <Form onSubmit={onSubmitSetPassword}>
      <Stack spacing={2}>
        <LegacyTextField
          value={password1}
          label={t("password_reset.new_password_hint")}
          type="password"
          setState={setPassword1}
        />
        <LegacyTextField
          value={password2}
          label={t("password_reset.repeat_password_hint")}
          type="password"
          setState={setPassword2}
        />
        {passwordResetErrors.map(error => (
          <Box width="100%">
            <PasswordValidationError error={error} />
          </Box>
        ))}
        <Box pt={2} width="100%">
          <Button type="submit">{t("password_reset.change_password")}</Button>
        </Box>
      </Stack>
    </Form>
  );
};

export function PasswordReset() {
  let { path, url } = useRouteMatch();

  return (
    <FullScreenCenteredLayout>
      <Switch>
        <Route exact path={path}>
          <RequestPasswordReset redirectUrl={`${url}/email-sent`} />
        </Route>
        <Route path={`${path}/:token`}>
          <SetNewPassword />
        </Route>
      </Switch>
    </FullScreenCenteredLayout>
  );
}
