import React from "react";
import Box from "@material-ui/core/Box";
import { Card } from "components/Card";
import { ArrowRight } from "components/icons/ArrowRight";
import { Heading } from "components/Heading";
import { Subheading } from "components/Subheading";
import { Text } from "components/Text";
import { Container } from "components/layouts/Container";
import { Columns } from "components/layouts/Columns";
import { Column } from "components/layouts/Column";
import { Stack } from "components/layouts/Stack";
import { Link } from "components/Link";
import { IframeLayout } from "components/layouts/IframeLayout";
import { useTranslation } from "react-i18next";

export const Join = () => {
  const { t } = useTranslation();

  return (
    <IframeLayout>
      <Container variant="wide">
        <Box mt={7} />
        <Heading size="lg" gutterBottom>
          {t("funnel.join.title")}
        </Heading>
        <Box maxWidth={600}>
          <Subheading gutterBottom>{t("funnel.join.subtitle")}</Subheading>
          <Text paragraph>{t("funnel.join.text_1")}</Text>
          <Text bold paragraph>
            {t("funnel.join.text_2")}
          </Text>
        </Box>
        <Box mt={8}>
          <Columns spacing={8}>
            <Column>
              <Link
                target="_parent"
                to="/product-picker?opting_for_role=member"
                showUnderline={false}
              >
                <Card
                  px={6}
                  cursor="pointer"
                  onClick={() =>
                    window.open(
                      "/product-picker?opting_for_role=member",
                      "_parent"
                    )
                  }
                  height="160px"
                  isHighlighted
                >
                  <Box
                    display="flex"
                    height="100%"
                    flexDirection="row"
                    justifyContent="space-between"
                  >
                    <Box
                      height="100%"
                      maxWidth={130}
                      display="flex"
                      justifyContent="start"
                    >
                      <Text bold size="lg">
                        {t("funnel.join.make_an_account")}
                      </Text>
                    </Box>
                    <Box color="primary.main">
                      <ArrowRight />
                    </Box>
                  </Box>
                </Card>
              </Link>
            </Column>
            <Column>
              <Link
                target="_parent"
                to="/product-picker?opting_for_role=sponsored"
                showUnderline={false}
              >
                <Card px={6} cursor="pointer" height="160px">
                  <Box
                    display="flex"
                    height="100%"
                    flexDirection="row"
                    justifyContent="space-between"
                  >
                    <Box
                      height="100%"
                      maxWidth={130}
                      display="flex"
                      justifyContent="start"
                    >
                      <Stack>
                        <Text bold size="lg">
                          {t("funnel.join.partner_sponsors_me")}
                        </Text>
                        <Text size="sm">{t("funnel.join.only_mobile")}</Text>
                      </Stack>
                    </Box>
                    <Box color="primary.main">
                      <ArrowRight />
                    </Box>
                  </Box>
                </Card>
              </Link>
            </Column>
          </Columns>
          <Box mt={4}>
            <Text color="primary.main">
              {t("funnel.join.i_have_an_account")}{" "}
              <Link target="_parent" to="/login?redirect_to=/product-picker">
                <strong>
                  <u>{t("common.login")}</u>
                </strong>
              </Link>
            </Text>
          </Box>
        </Box>
        <Box mb={9} />
      </Container>
    </IframeLayout>
  );
};
