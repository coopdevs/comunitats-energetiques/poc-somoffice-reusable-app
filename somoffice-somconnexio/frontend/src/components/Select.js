import { MenuItem, withStyles } from "@material-ui/core";
import { Select as MaterialSelect } from "@material-ui/core";
import FormHelperText from "@material-ui/core/FormHelperText";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import {
  StyledFormControl,
  StyledInputLabel,
  important
} from "./shared/InputCustomizations";
import { Field } from "react-final-form";
import Box from "@material-ui/core/Box";
import React from "react";
import "./Select.css";

const CaretIcon = ({ direction = "down" }) => (
  <svg
    width="20"
    height="10"
    viewBox="0 0 20 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    style={{
      cursor: "pointer",
      transformOrigin: "center center",
      transform: direction === "down" && "rotate(180deg)"
    }}
  >
    <path d="M19 9L10 0.999999L1 9" stroke="currentColor" />
  </svg>
);

/**
 * Changes text color inside select drop down
 */
const useStyles = makeStyles(theme => ({
  paper: {
    color: important(theme.palette.text.main),
    maxHeight: "400px"
  }
}));

const StyledSelect = withStyles(theme => ({
  root: {
    color: theme.palette.text.main,
    width: "100%"
  },
  select: {
    "&:focus": {
      backgroundColor: "transparent"
    }
  }
}))(MaterialSelect);

const inferIsOpen = classes => Boolean(classes.match(/MuiSelect-iconOpen/));

export const Select = ({
  options,
  label,
  helperText,
  error,
  i18nPrefix,
  ...props
}) => {
  const styles = useStyles();
  const { t } = useTranslation();

  const getOptions = () => {
    if (!i18nPrefix) {
      return options;
    }

    return options.map(value => ({
      label: t([i18nPrefix, props.name, value].join(".")),
      value
    }));
  };

  return (
    <>
      <StyledFormControl variant="outlined" error={error}>
        <StyledInputLabel>{label}</StyledInputLabel>
        <StyledSelect
          {...props}
          label={label}
          MenuProps={{
            classes: {
              paper: styles.paper
            }
          }}
          IconComponent={({ className }) => (
            <Box color="text.main" pr={4}>
              <CaretIcon direction={inferIsOpen(className) ? "up" : "down"} />
            </Box>
          )}
        >
          {getOptions().map(({ value, label }, index) => (
            <MenuItem key={JSON.stringify(value)} value={value}>
              <div style={{ overflow: 'hidden', textOverflow: 'ellipsis' }}>
                {label}
              </div>
            </MenuItem>
          ))}
        </StyledSelect>
        {helperText && <FormHelperText>{helperText}</FormHelperText>}
      </StyledFormControl>
    </>
  );
};

const FormField = ({ name, validate, ...props }) => {
  const { t } = useTranslation();

  return (
    <Field name={name} validate={validate}>
      {({ input, meta }) => (
        <Select
          {...props}
          error={meta.error}
          name={input.name}
          value={input.value}
          onChange={input.onChange}
          helperText={
            meta.submitFailed && meta.error && t(`common.errors.validation.${meta.error}`)
          }
        />
      )}
    </Field>
  );
};

Select.FormField = FormField;
