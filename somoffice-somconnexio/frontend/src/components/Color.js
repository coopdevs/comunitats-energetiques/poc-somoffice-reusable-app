import { Text } from "components/Text";
import React from "react";
import Box from "@material-ui/core/Box";

/**
 * This component is only used in storybook
 */

export const Color = ({ color, border }) => (
  <Box>
    <Box
      border={border && "1px solid black"}
      borderRadius={5}
      width="48px"
      height="48px"
      bgcolor={color}
    />
    <Box>
      <Text size="xs">{color}</Text>
    </Box>
  </Box>
);
