import React, { useState } from "react";
import MaterialTextField from "@material-ui/core/TextField";
import { Box, withStyles, Tooltip as MaterialTooltip } from "@material-ui/core";

import { Tooltip } from "components/Tooltip";
import { StyledFormControl } from "./shared/InputCustomizations";
import { Field } from "react-final-form";
import { useTranslation } from "react-i18next";
import { IMaskInput } from "react-imask";

const StyledTextField = withStyles(theme => ({
  root: {
    color: theme.palette.text.main
  }
}))(MaterialTextField);

const InfoIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM11 15H9V9H11V15ZM11 7H9V5H11V7Z"
      fill="#B1A76E"
    />
  </svg>
);

const MaskedInput = props => {
  const {
    onChange,
    placeholder,
    showPlaceHolderMask = false,
    mask,
    ...other
  } = props;
  const [hasBeenFocused, setHasBeenFocused] = useState(false);

  return (
    <IMaskInput
      {...other}
      placeholder={placeholder}
      mask={mask}
      onFocus={(...args) => {
        setHasBeenFocused(true);
        props.onFocus && props.onFocus(...args);
      }}
      lazy={!(showPlaceHolderMask && hasBeenFocused)}
      mapToRadix={["/", ".", "-", " "]}
      onAccept={value => onChange({ target: { name: props.name, value } })}
      overwrite
    />
  );
};

/**
 * Preconfigured Material UI TextInput for consistency sake
 */
export const TextField = React.forwardRef(({ label, info, ...props }, ref) => (
  <Box display="flex" flexDirection="row">
    <StyledFormControl>
      <StyledTextField
        {...props}
        color="text.main"
        label={label}
        variant="outlined"
        InputProps={
          props.mask
            ? {
                inputComponent: MaskedInput,
                inputProps: props
              }
            : undefined
        }
        inputRef={ref}
        fullWidth
      />
    </StyledFormControl>

    {info && (
      <Box display="flex" alignItems="center" justifyContent="center" ml={2}>
        <Tooltip
          variant="info"
          interactive
          enterTouchDelay={0}
          leaveTouchDelay={0}
          PopperProps={{
            disablePortal: true
          }}
          title={
            <React.Fragment>
              <div>{info}</div>
            </React.Fragment>
          }
          arrow
        >
          <div>
            <InfoIcon />
          </div>
        </Tooltip>
      </Box>
    )}
  </Box>
));

const helperText = (t, meta) => {
  const error = meta.error || meta.submitError;

  const [errorKey, errorTextInterpolationValue] = Array.isArray(error)
    ? error
    : [error];

  return (
    meta.submitFailed &&
    error &&
    t(`common.errors.validation.${errorKey}`, errorTextInterpolationValue)
  );
};

const FormField = React.forwardRef(({ name, validate, ...props }, ref) => {
  const { t } = useTranslation();

  return (
    <Field name={name} validate={validate}>
      {({ input, meta }) => (
        <>
          <TextField
            {...props}
            error={meta.error}
            name={input.name}
            value={input.value}
            onChange={input.onChange}
            ref={ref}
            helperText={helperText(t, meta)}
          />
        </>
      )}
    </Field>
  );
});

TextField.FormField = FormField;
