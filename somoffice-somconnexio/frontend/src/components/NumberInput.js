import React, { useCallback } from "react";
import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core";

const Circle = styled("div")(({ theme }) => ({
  border: `1px solid ${theme.palette.text.main}`,
  opacity: ({ disabled }) => (disabled ? 0.2 : 1.0),
  width: 40,
  height: 40,
  borderRadius: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer"
}));

const NumberBox = styled("div")(({ theme }) => ({
  border: `1px solid ${theme.palette.text.main}`,
  backgroundColor: "white",
  borderRadius: 10,
  width: 50,
  height: 50,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontSize: 22,
  margin: "0 15px",
  userSelect: "none"
}));

const PlusShape = () => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <line y1="7.5" x2="16" y2="7.5" stroke="currentColor" />
      <line x1="8.5" y1="2.18557e-08" x2="8.5" y2="16" stroke="currentColor" />
    </svg>
  );
};

const MinusShape = () => {
  return (
    <svg
      width="16"
      height="1"
      viewBox="0 0 16 1"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <line y1="0.5" x2="16" y2="0.5" stroke="currentColor" />
    </svg>
  );
};

const preventTextSelection = e => e.preventDefault();

export const NumberInput = ({ value, onChange = () => {}, min = 0, max }) => {
  const isMinDisabled = min === undefined ? false : value === min;
  const isMaxDisabled = max === undefined ? false : value === max;

  const triggerChange = useCallback(
    (delta, isDisabled) => event => {
      if (isDisabled) {
        return;
      }

      onChange(value + delta);
    },
    [value, onChange]
  );

  return (
    <Box
      onMouseDown={preventTextSelection}
      display="flex"
      flexDirection="row"
      alignItems="center"
      color="text.main"
    >
      <Circle
        onClick={triggerChange(-1, isMinDisabled)}
        disabled={isMinDisabled}
      >
        <MinusShape />
      </Circle>
      <NumberBox>{value}</NumberBox>
      <Circle
        onClick={triggerChange(1, isMaxDisabled)}
        disabled={isMaxDisabled}
      >
        <PlusShape />
      </Circle>
    </Box>
  );
};
