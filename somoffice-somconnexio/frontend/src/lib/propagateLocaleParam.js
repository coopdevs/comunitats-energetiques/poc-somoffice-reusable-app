/**
 * If locale is set in the url, we need to propagate to the destination url (if
 * it is a relative url)
 */
export const propagateLocaleParam = to => {
  const isExternalLink = Boolean(to.match(/^https?:\/\/|^mailto:/));
  const localeFromUrl = new URLSearchParams(window.location.search).get(
    "locale"
  );

  if (isExternalLink || !localeFromUrl) {
    return to;
  }

  const [, queryString] = to.split("?");
  const params = new URLSearchParams(queryString);
  params.set("locale", localeFromUrl);

  return to.replace(/\?.*$/, `?${params.toString()}`);
};
