import { getSubscriptionList } from "lib/api/subscriptions";
import { useQuery } from "react-query";

export const useSubscriptions = (options) =>
  useQuery("subscriptions", async () => (await getSubscriptionList()).data, options);
