import React, { useState, useEffect, Suspense } from "react";
import "./App.css";

import i18next from "i18next";

import CssBaseline from "@material-ui/core/CssBaseline";
import { Switch, Route, Redirect, useLocation } from "react-router-dom";

import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "./theme";
import { initialize } from "./initializers/";
import { FullScreenCenteredLayout } from "./components/layouts/FullScreenCenteredLayout";
import { ApplicationContext } from "./lib/ApplicationContext";
import { whoami } from "./lib/api/auth";
import { useApplicationContext } from "./hooks/useApplicationContext";

import { MaintenanceMode } from "./screens/MaintenanceMode";
import { Home } from "./screens/Home/";
import { Login } from "./screens/Login";
import { Invoices } from "./screens/Invoices";
import { RequestChange } from "./screens/RequestChange";
import { PasswordReset } from "./screens/PasswordReset";
import { Join } from "./screens/Join";
import { ProductPicker } from "./screens/ProductPicker";
import { Profile } from "./screens/Profile";
import { Tariffs } from "./screens/Tariffs";
import { SignUp } from "./screens/SignUp";
import { ConfirmEmail } from "./screens/ConfirmEmail";
import { DevIframe } from "./screens/DevIframe";
import { Spinner } from "./components/Spinner";
import { QueryClient, QueryClientProvider, useQuery } from "react-query";
import { QueryParamProvider } from "use-query-params";
import { useHistory } from 'react-router-dom';
import "console-goodies";

const queryClient = new QueryClient();

/**
 * Flip this boolean to enable/disable maintenance mode and deploy.
 */
const isMaintenanceMode = false;

const SplashScreen = () => (
  <FullScreenCenteredLayout>
    <Spinner />
  </FullScreenCenteredLayout>
);

const PrivateRoute = ({ children, ...rest }) => {
  const { currentUser } = useApplicationContext();
  const isAuthenticated = Boolean(currentUser);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
};

const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};

const AppRoutes = ({ onLogin }) => {
  if (isMaintenanceMode) {
    return <MaintenanceMode />;
  }

  return (
    <>
      <ScrollToTop />
      <Switch>
        <Route path="/login">
          <Login onLogin={onLogin} />
        </Route>

        <Route path="/password-reset">
          <PasswordReset />
        </Route>

        <Route path="/confirm-email/:token">
          <ConfirmEmail />
        </Route>

        <PrivateRoute path="/home">
          <Home />
        </PrivateRoute>

        <PrivateRoute path="/profile">
          <Profile />
        </PrivateRoute>

        <PrivateRoute path="/invoices">
          <Invoices />
        </PrivateRoute>

        <PrivateRoute path="/request-change">
          <RequestChange />
        </PrivateRoute>

        {/* funnel routes */}

        <Route path="/join">
          <Join />
        </Route>

        <Route path="/tariffs/:type">
          <Tariffs />
        </Route>

        <Route path="/product-picker">
          <ProductPicker />
        </Route>

        <Route path="/signup">
          <SignUp />
        </Route>

        <Route path="/__iframe">
          <DevIframe />
        </Route>

        <Route path="*" render={() => <Redirect to="/home" />} />
      </Switch>
    </>
  );
};

function App() {
  const [isInitializing, setIsInitializing] = useState(true);
  const [contextValue, setContextValue] = useState({ currentUser: null });
  const history = useHistory();

  const getCurrentUser = async () => {
    let currentUser;
    try {
      currentUser = await whoami();
    } catch {
      currentUser = null;
    }

    return currentUser;
  };

  const onLogin = async () => {
    const currentUser = await getCurrentUser();
    setContextValue({ currentUser });
  };

  useEffect(() => {
    (async () => {
      await initialize();
      const currentUser = await getCurrentUser();

      if (currentUser) {
        setContextValue({ currentUser });
      } else {
        setIsInitializing(false);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const { currentUser } = contextValue;
      const localeFromUrl = new URLSearchParams(window.location.search).get('locale')

      if (!currentUser) {
        return;
      }

      if (!Boolean(localeFromUrl)) {
        await i18next.changeLanguage(currentUser.preferred_locale);
      }

      setIsInitializing(false);
    })();
  }, [contextValue]);

  useEffect(() => {
    function trackMatomo(url) {
      const _paq = window._paq || [];
      _paq.push(['setCustomUrl', url]);
      _paq.push(['trackPageView', url]);
    }

    function trackGoogleAnalytics(url) {
      const ga = window.ga || function () { };

      ga('set', 'page', url);
      ga('send', 'pageview');
    }

    history.listen((location) => {
      const trackUrl = location.pathname + (location.search || '');

      trackMatomo(trackUrl);
      trackGoogleAnalytics(trackUrl);
    })
  }, [history])

  return (
    <>
      <Suspense fallback={() => <SplashScreen />}>
        <ApplicationContext.Provider value={{ contextValue, setContextValue }}>
          <QueryParamProvider ReactRouterRoute={Route}>
            <QueryClientProvider client={queryClient}>
              <ThemeProvider theme={theme}>
                <CssBaseline />
                {!isMaintenanceMode && isInitializing ? (
                  <SplashScreen />
                ) : (
                  <AppRoutes onLogin={onLogin} />
                )}
              </ThemeProvider>
            </QueryClientProvider>
          </QueryParamProvider>
        </ApplicationContext.Provider>
      </Suspense>
    </>
  );
}

export default App;
