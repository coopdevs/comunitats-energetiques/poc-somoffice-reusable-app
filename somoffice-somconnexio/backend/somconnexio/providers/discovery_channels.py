from odoo_somconnexio_python_client.resources.discovery_channel import DiscoveryChannel
from somconnexio.serializers.discovery_channel import DiscoveryChannelSerializer


class DiscoveryChannelsProvider:
    def __init__(self, options):
        self.options = options

    def get_resources(self):
        language = self.options["language"]

        discovery_channels = DiscoveryChannel.search(lang=language)
        serializer = DiscoveryChannelSerializer(discovery_channels, many=True)

        return serializer.data
