from pyopencell.resources.customer import Customer
from odoo_somconnexio_python_client.resources.partner import Partner
from somconnexio.domain.customer import get_customer_resource
from somconnexio.presenters.profile import Profile
from somconnexio.domain.keycloak import KeycloakRemoteUserBackend
from somconnexio.serializers.profile import ProfileSerializer
from somconnexio.providers.tickets import TicketsProvider


class ProfileProvider:
    def __init__(self, options):
        print(f"SomConnexió SomOffice - Creating resource profile")
        self.options = options

    def get_resource(self):
        customer_code = self.options["remote_user_id"]

        keycloak_user = self._get_keycloak_user()

        customer_resource = (
            get_customer_resource(customer_code) or self._get_null_customer_resource(keycloak_user)
        )

        current_user = self.options["current_user"]
        profile = Profile(
            customer=customer_resource,
            current_user=current_user,
            keycloak_user=keycloak_user,
            role=self._get_user_role(current_user.username)
        )

        serializer = ProfileSerializer(profile)

        return serializer.data

    def request_email_change(self, new_email):
        tickets_provider = TicketsProvider()

        tickets_provider.create(
            user=self.options["current_user"],
            meta=[
                {
                    "key": "ticket_type",
                    "value": "change_personal_email"
                },
                {
                    "key": "email",
                    "value": new_email
                }
            ]
        )

    def change_email(self, new_email):
        try:
            KeycloakRemoteUserBackend().update_user(
                self._get_keycloak_user(), {"email": new_email},
            )
        except Exception as e:
            print("An error ocurrred when trying to change user email")
            print(e)
            return False

        return True

    def _get_null_customer_resource(self, keycloak_user):
        name = {
            "firstName": keycloak_user['email'],
            "lastName": "",
        }
        return Customer(name=name)

    def _get_keycloak_user(self):
        username = self.options["current_user"].username
        return KeycloakRemoteUserBackend().get_keycloak_user(username)

    def _get_user_role(self, vat):
        partner = Partner.search_by_vat(vat)
        if partner.member or partner.coop_candidate:
            return "member"
        elif partner.sponsor_id:
            return "sponsored"
        elif partner.coop_agreement_code:
            return "coop_agreement"
