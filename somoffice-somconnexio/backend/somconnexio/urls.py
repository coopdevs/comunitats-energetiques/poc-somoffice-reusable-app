from django.urls import path
from . import views

base_path = "api/admin/"

urlpatterns = [
    path(base_path + "import_user/", views.import_user),
    path(base_path + "user/", views.get_user),
    path(base_path + "change_user_email", views.change_user_email),
]
