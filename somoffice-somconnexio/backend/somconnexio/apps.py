from django.apps import AppConfig


class SomconnexioAdminConfig(AppConfig):
    name = 'somconnexio_admin'
