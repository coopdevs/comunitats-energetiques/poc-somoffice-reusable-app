from rest_framework import serializers


class DiscoveryChannelSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
