
from rest_framework import serializers


class AvailableProviderSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
