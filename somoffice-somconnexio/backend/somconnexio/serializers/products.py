from rest_framework import serializers


class ProductsSerializer(serializers.Serializer):
    code = serializers.CharField()
    description = serializers.CharField()
    price = serializers.CharField()
    min_included = serializers.CharField()
    data_included = serializers.CharField()
