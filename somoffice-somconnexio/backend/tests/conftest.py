import pytest


def scrub_cookies(response):
    response["headers"]["Cookie"] = "****"
    response["headers"]["Set-Cookie"] = "****"
    return response


@pytest.fixture(scope="module")
def vcr_config():
    return {
        "filter_headers": [("Authorization", "****"), ("API-KEY", "****")],
        "before_record_response": scrub_cookies,
    }
