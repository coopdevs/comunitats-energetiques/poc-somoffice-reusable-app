import pytest
from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse
from django.utils.http import urlencode


class TestUsers(APITestCase):
    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_partner_search_by_vat_empty(self):

        response = self.client.get("/api/user/exists/")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_partner_search_by_vat_invalid(self):
        params = {"vat": "XXX"}
        url = "{}?{}".format(reverse("user_exists"), urlencode(params))
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_partner_search_by_vat_valid(self):
        params = {"vat": "ESQ2826000H"}
        url = "{}?{}".format(reverse("user_exists"), urlencode(params))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
