import pytest
from somoffice.models import User, Profile
from rest_framework.test import APIClient
from somoffice.core.secure_resources import build_secure_id_generator


class TestSubscriptions:
    def test_subscriptions_without_session(self):
        secure_id_generator = build_secure_id_generator(
            user_id="1859", resource_type="subscription"
        )

        client = APIClient()
        response = client.get(f"/api/subscriptions/{secure_id_generator('220')}/")

        assert response.status_code == 403

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_subscription_that_does_not_belong_to_user(self):
        client, secure_id_generator = self._authenticate()

        secure_id_generator = build_secure_id_generator(
            user_id="another-user-id", resource_type="subscription"
        )

        response = client.get(f"/api/subscriptions/{secure_id_generator('220')}/")

        assert response.status_code == 403

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_subscriptions_index(self):
        client, secure_id_generator = self._authenticate()

        response = client.get(f"/api/subscriptions/")

        assert response.status_code == 200

    def _authenticate(self):
        user = User.objects.create(username="55771114X")
        Profile.objects.create(
            remoteBackend="opencell", remoteId="1017", preferredLocale="ca", user=user
        )

        secure_id_generator = build_secure_id_generator(
            user_id="1017", resource_type="subscription"
        )

        client = APIClient()

        client.force_authenticate(user=user)

        return (client, secure_id_generator)
