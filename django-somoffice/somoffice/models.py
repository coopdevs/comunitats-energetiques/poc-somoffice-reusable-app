from django.contrib.auth.models import AbstractUser
from django.contrib import auth
from django.db import models


class User(AbstractUser):
    # TODO document why
    def set_password(self, password):
        self._auth_backend().change_password(self.username, password)

    def check_password(self, password):
        return self._auth_backend().check_password(self.username, password)

    def _auth_backend(self):
        # TODO limit to one auth backend? hmmm
        return auth.get_backends()[0]


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    remoteBackend = models.CharField(max_length=256)
    remoteId = models.CharField(max_length=256)
    preferredLocale = models.CharField(max_length=5, null=True)

class EmailConfirmationToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    token = models.CharField(max_length=64, null=False)
    expiresAt = models.DateTimeField(null=False)
    email = models.EmailField(null=False, max_length=254)
