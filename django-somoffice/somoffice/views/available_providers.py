from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from somoffice.core import load_resource_provider


class AvailableProviders(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request):

        category = request.query_params.get("category")
        if not category:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            options = {"category": category}
            provider = load_resource_provider("available_providers")(options)

            return Response(provider.get_resources())
