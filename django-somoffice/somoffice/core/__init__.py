from django.utils.module_loading import import_string
from django.conf import settings


def load_resource_provider(resource_type):
    print(f"SomOffice Django - Loading resource {resource_type}")
    return import_string(
        settings.SOMOFFICE_RESOURCE_PROVIDERS[resource_type]
    )
